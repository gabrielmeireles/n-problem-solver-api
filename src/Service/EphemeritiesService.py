import datetime
import matplotlib.pyplot as plot
from src.Repository.NASAhorizons import NASAhorizons
import concurrent.futures
from src.Service.BodiesService import add_ephemerities_to_body
from src.util.decorators import log_time


@log_time
def get_body_ephemerities(body):
    date = datetime.date(year=2020, month=1, day=1)
    jpl = NASAhorizons()
    jpl.set_object_id(int(body.body_id))
    ephemerities = jpl.get_data(date)
    add_ephemerities_to_body(ephemerities, body)
    print(body)
    return body


@log_time
def get_ephemerities_nasa(bodies_search_list):
    bodies_with_ephemerities = []
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for body in executor.map(get_body_ephemerities, bodies_search_list):
            bodies_with_ephemerities.append(body)
    #plot_output(bodies_with_ephemerities)
    return bodies_with_ephemerities


def plot_output(bodies):
    for body in bodies:
        plot.scatter(body.position[0], body.position[1])
    plot.show()
