import json
import numpy as np

from src.DTO.BodyDTO import BodyDTO

JSON_PATH = "./resources/bodies.json"


def get_known_body(body_id):
    with open(JSON_PATH, 'r') as json_file:
        file = json_file.read()
        data = json.loads(file)
        body_json = data[str(body_id)]

    body_name = body_json["name"]
    body_mass = body_json["mass"]
    body_radius = body_json["radius"]

    if body_id == 0:
        position = np.array([body_json["x"], body_json["y"], body_json["z"]])
        velocity = np.array([body_json["vx"], body_json["vy"], body_json["vz"]])
        body = BodyDTO(
            body_id,
            body_name,
            body_mass,
            body_radius,
            position,
            velocity,
            None)
    else:
        body = BodyDTO(
            body_id,
            body_name,
            body_mass,
            body_radius,
            None,
            None,
            None)
    return body


def add_ephemerities_to_body(data, body: BodyDTO):
    position = np.array([data["x"], data["y"], data["z"]])
    velocity = np.array([data["vx"], data["vy"], data["vz"]])
    body.add_ephemerities(position, velocity)
