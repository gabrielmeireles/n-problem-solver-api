import numpy as np
from scipy.integrate import RK45
import matplotlib.pyplot as plot


class NBodyProblemSolver:
    def __init__(self, bodies):
        self.bodies = bodies
        self.g = 6.6742e-20
        self.number_bodies = len(bodies)

    def update_bodies(self, state_components):
        reshaped_components = np.reshape(state_components, (self.number_bodies, 6))
        for index, body in enumerate(self.bodies):
            body_components = reshaped_components[index]

            body.position = body_components[0:3]

            body.x_trajectory = np.append(body.x_trajectory, body_components[0])
            body.y_trajectory = np.append(body.y_trajectory, body_components[1])
            body.z_trajectory = np.append(body.z_trajectory, body_components[2])

    def integrate_acceleration(self, time, state_vector):
        new_state = np.array([])
        reshaped_components = np.reshape(state_vector, (self.number_bodies, 6))
        for index, main_body in enumerate(self.bodies):
            final_acceleration = np.array([0, 0, 0])
            for body in self.bodies:
                if body.body_id != main_body.body_id:
                    r = body.position - main_body.position
                    acceleration = self.g * body.mass * r / (np.linalg.norm(r) ** 3)
                    final_acceleration = np.sum([final_acceleration, acceleration], axis=0)
            new_state = np.append(new_state, reshaped_components[index][3:6])
            new_state = np.append(new_state, final_acceleration)
        return new_state

    def simulate(self):
        simulation_time = 365 * 24 * 60 * 60
        initial_state = np.array([])
        for body in self.bodies:
            initial_state = np.append(initial_state, body.position)
            initial_state = np.append(initial_state, body.velocity)
        integrator = RK45(self.integrate_acceleration, t0=0.0, y0=initial_state, t_bound=simulation_time, max_step=6*3600)

        t = 0
        while t < simulation_time and integrator.status == 'running':
            integrator.step()
            self.update_bodies(integrator.y)
            t = integrator.t

    def plot_output(self):
        fig = plot.figure()
        colours = ['r', 'b', 'g', 'y', 'm', 'c', 'k', 'grey', 'orange', 'gold', 'olive', 'indigo']
        ax = fig.add_subplot(1, 1, 1, projection='3d')
        max_range = 0
        for index, current_body in enumerate(self.bodies):
            max_dim = max(max(current_body.x_trajectory), max(current_body.y_trajectory), max(current_body.z_trajectory))
            if max_dim > max_range:
                max_range = max_dim
            ax.plot(current_body.x_trajectory, current_body.y_trajectory, current_body.z_trajectory, c=colours[index],
                    label=current_body.name)

        ax.set_xlim([-max_range, max_range])
        ax.set_ylim([-max_range, max_range])
        ax.set_zlim([-max_range, max_range])
        ax.legend()
        plot.show()
