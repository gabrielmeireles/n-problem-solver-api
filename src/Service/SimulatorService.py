from src.Service.BodiesService import get_known_body
from src.Service.EphemeritiesService import get_ephemerities_nasa
from src.Service.NBodyProblemSolver import NBodyProblemSolver


def n_body_problem_simulator(data):
    known_bodies = data['knownBodies']
    unknown_bodies = data['unknownBodies']

    bodies_list = []
    search_list = []

    for body_id in known_bodies:
        body = get_known_body(body_id)
        if body_id == 0:
            bodies_list.append(body)
        else:
            search_list.append(body)

    bodies_list.extend(get_ephemerities_nasa(search_list))

    solver = NBodyProblemSolver(bodies_list)
    solver.simulate()
    solver.plot_output()

    return bodies_list
