from json import JSONEncoder

import numpy as np

from src.util.converter import convert_positions, convert_velocities


class BodyDTO:
    def __init__(self, body_id, name=None, mass=None, radius=None, position=None, velocity=None, reference_body=None):
        """
        Class constructor

        Parameters:
            mass: float. Body mass in kg
            position: list or numpy array. [X, Y, Z] position of the body in km
            velocity: list or numpy array. [VX, VY, VZ] velocity of the body in km/s
            radius: float. Body radius in km
        """
        self.body_id = body_id
        self.name = name
        self.mass = mass
        self.radius = radius

        """
        Converting positions and velocities to Sun reference.
        """

        if reference_body:
            convert_positions(self, reference_body)
            convert_velocities(self, reference_body)
        else:
            self.position = position
            self.velocity = velocity

        """
        Parameters defining the trajectory of the body during the simulation.
        """

        if position is not None and velocity is not None:
            self.x_trajectory = np.array([self.position[0]])
            self.y_trajectory = np.array([self.position[1]])
            self.z_trajectory = np.array([self.position[2]])

    def __str__(self):
        return "Name: {},Mass: {},Position: {},Velocity: {},Radius: {}" \
            .format(str(self.name), str(self.mass), self.position, self.velocity, str(self.radius))

    def add_ephemerities(self, position, velocity):
        self.position = position
        self.velocity = velocity
        self.x_trajectory = np.array([position[0]])
        self.y_trajectory = np.array([position[1]])
        self.z_trajectory = np.array([position[2]])


class BodyDTOEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, BodyDTO):
            return obj.__dict__
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)
