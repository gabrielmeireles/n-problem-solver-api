import numpy as np


def convert_positions(body, reference_body):
    if reference_body.reference_body:
        convert_positions(reference_body, reference_body.reference_body)
        reference_body.reference_body = None

    new_position_coordinate = convert_coordinates(body.position, reference_body.position)
    body.position = new_position_coordinate


def convert_velocities(body, reference_body):
    if reference_body.reference_body:
        convert_velocities(reference_body, reference_body.reference_body)
        reference_body.reference_body = None

    new_velocity_coordinate = convert_coordinates(body.velocity, reference_body.velocity)
    body.velocity = new_velocity_coordinate


def convert_coordinates(coordinates, reference_coordinates):
    coordinates_list = [coordinates, reference_coordinates]
    return np.add.reduce(coordinates_list)
