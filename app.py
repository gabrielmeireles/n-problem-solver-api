import json

from flask import Flask, Response, render_template
from flask import request
from flask_swagger_ui import get_swaggerui_blueprint
from src.DTO.BodyDTO import BodyDTOEncoder
from src.Service.BodiesService import get_known_body
from src.Service.EphemeritiesService import get_body_ephemerities
from src.Service.SimulatorService import n_body_problem_simulator

app = Flask(__name__)

SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "N-Body-Problem Api UFMG"
    }
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)


@app.route('/ephemerities/<body_id>')
def get_ephemerities(body_id):
    body = get_known_body(body_id)
    get_body_ephemerities(body)
    return '<span style="white-space: pre-line">'+body.__str__()+'</span>'


@app.route('/simulate', methods=['POST'])
def simulate():
    data = request.get_json(force=True)
    updated_data = n_body_problem_simulator(data)
    return Response(json.dumps(updated_data, cls=BodyDTOEncoder), mimetype='application/json')


@app.route("/")
def home_view():
    return "<h1>Home Page</h1>"


@app.route('/healthCheck')
def health_check():
    return 'ok'


if __name__ == '__main__':
    app.run()
