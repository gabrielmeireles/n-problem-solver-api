import enum


class Planets(enum.Enum):
    Mercury = 199
    Venus = 299
    Earth = 399
    Mars = 499
    Jupiter = 599
    Saturn = 699
    Uranus = 799
    Neptune = 899
    Pluto = 999

    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))


class Satellites(enum.Enum):
    Moon = 301
